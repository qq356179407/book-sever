//引入express 
const express = require('express')
const managerRouter = require('./routes/manager');
const bookRouter = require('./routes/book');
const borrowRouter = require('./routes/borrow');
const returnRouter = require('./routes/return');
const studentRouter = require('./routes/student');
const login = require('./routes/login');
const bodyParser = require('body-parser')
// const expressJWT = require('express-jwt');

//创建服务
const app = express()

//parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended:false}))
//parse application/json
app.use(bodyParser.json())

//设置跨域访问
app.all("*",function(req,res,next){
    //设置允许跨域的域名，*代表允许任意域名跨域
    res.header("Access-Control-Allow-Origin","*");
    //允许的header类型
    res.header("Access-Control-Allow-Headers","content-type,authorization");
    //跨域允许的请求方式 
    res.header("Access-Control-Allow-Methods","DELETE,PUT,POST,GET,OPTIONS");
    if (req.method.toLowerCase() == 'options')
        res.send(200);  //让options尝试请求快速结束
    else
        next();
})


//注册路由
app.use('/manager', managerRouter);
app.use('/book', bookRouter);
app.use('/student', studentRouter);
app.use('/return', returnRouter);
app.use('/borrow', borrowRouter);
app.use('/', login);




//监听项目端口
app.listen(3000,function(){
})



