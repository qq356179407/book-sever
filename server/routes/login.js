var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var mysql = require("../mysql/Config");
var token = require('../model/token');
const saltRounds = 10;
const salt = bcrypt.genSaltSync(saltRounds);
router.all("*",function(req,res,next){
    //设置允许跨域的域名，*代表允许任意域名跨域
    res.header("Access-Control-Allow-Origin","*");
    //允许的header类型
    res.header("Access-Control-Allow-Headers","content-type");
    //跨域允许的请求方式 
    res.header("Access-Control-Allow-Methods","DELETE,PUT,POST,GET,OPTIONS");
    if (req.method.toLowerCase() == 'options')
        res.send(200);  //让options尝试请求快速结束
    else
        next();
})


//登录
router.post('/login', function(req, res, next) {
	let param = [req.body.manage_username,req.body.manage_password]
	console.log(param)
	let SQL = `SELECT manage_password FROM manage where manage_username = '${param[0]}'`
	let btoken = token.token.generateToken(param[0])
	mysql.query(SQL, function (err, results) {
	    if (results) {
			if(bcrypt.compareSync(param[1], results[0].manage_password)) {
				res.json({
					status: 200,
					message: "成功",
					token:btoken
				})
			}else {
				res.json({
					status: 422,
					message: "密码错误"
				})
			}
			 
	    }else{
			res.json({
				status: 422,
				message: "该用户名不存在"
			})
		}
	})
	
});

//注册
router.post('/registered', function(req, res, next) {
	req.body.manage_password = bcrypt.hashSync(req.body.manage_password, salt)
	let param = [req.body.manage_name, req.body.manage_username, req.body.manage_password];
	let SQL = `INSERT INTO manage(manage_name, manage_username, manage_password) values('${param[0]}','${param[1]}','${param[2]}')`
	let cSQL = `SELECT * FROM manage where manage_username = '${param[1]}'`
	mysql.query(cSQL, function (err, results) {
		if(!results[0]) {
			mysql.query(SQL, function (err, results) {
			    if(results){
					res.json({
						status: 200,
						message: "注册成功"
					})
				}
				if(err){
					console.log(err);
				}
			})
		}else{
			res.json({
				status: 422,
				message: "用户名已注册"
			})
		}
	})
	
});


module.exports = router