var express = require('express');
var router = express.Router();
var token = require('../model/token')
//导入封装的数据库模块
var mysql = require("../mysql/Config");


//设置跨域访问
// router.all("*",function(req,res,next){
//     //设置允许跨域的域名，*代表允许任意域名跨域
//     res.header("Access-Control-Allow-Origin","*");
//     //允许的header类型
//     res.header("Access-Control-Allow-Headers","content-type");
//     //跨域允许的请求方式 
//     res.header("Access-Control-Allow-Methods","DELETE,PUT,POST,GET,OPTIONS");
//     if (req.method.toLowerCase() == 'options')
//         res.send(200);  //让options尝试请求快速结束
//     else
//         next();
// })


//定义路由


/**
 * @api {Get} /student 获取第一页学生信息
 * @apiGroup student
 *
 * @apiParam {int} num 每页数量
 * @apiParamExample {json} Request-Example
 * {
 *  "num": 3
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * 	"count": 3,
 *  "list": [
 *      {
 *          "student_id": 20170001,
 *          "student_username": "sdawe4",
 *          "student_password": "d878asd",
 *          "student_name": "江",
 *          "student_grade": "信科201803",
 *          "student_email": "154845@qq.com",
 *          "student_state": 1,
 *          "student_del": 1
 *      },
 *      {
 *          "student_id": 20170003,
 *          "student_username": "ss",
 *          "student_password": "12321",
 *          "student_name": "王德发",
 *          "student_grade": "计科201705",
 *          "student_email": "465616482@qq.com",
 *          "student_state": 1,
 *          "student_del": 1
 *      }
 *  ]
 * }
 */
router.get('/', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.num];
		let SQL = `SELECT * FROM student limit 0,${param[0]}`
		let cSQL = `SELECT COUNT(*) AS count FROM student`
		let count
		mysql.query(cSQL, function (err, results) {
		    if (results) {
				count = Math.ceil(results[0].count/param[0])
				mysql.query(SQL, function (err, results) {
				    if (results) {
				        res.json({
							"count":count,
							"list":results
						})
				    }
				})
		    }
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
	
    
})


/**
 * @api {Get} /student/page 按页数获取学生信息
 * @apiGroup student
 *
 * @apiParam {String} condition 条件
 * @apiParam {String} val 内容
 * @apiParam {int} page 页数
 * @apiParam {int} num 每页数量
 * @apiParamExample {json} Request-Example
 * {
 *  "condition": 'student_id'
 *  "val": '20170001'
 *  "page": 2
 *  "num": 3
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * "list": [
 *      {
 *          "student_id": 20170001,
 *          "student_username": "sdawe4",
 *          "student_password": "d878asd",
 *          "student_name": "江",
 *          "student_grade": "信科201803",
 *          "student_email": "154845@qq.com",
 *          "student_state": 1,
 *          "student_del": 1
 *      },
 *      {
 *          "student_id": 20170003,
 *          "student_username": "ss",
 *          "student_password": "12321",
 *          "student_name": "王德发",
 *          "student_grade": "计科201705",
 *          "student_email": "465616482@qq.com",
 *          "student_state": 1,
 *          "student_del": 1
 *      }
 *  ]
 * }
 */
router.get('/page', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.condition, req.query.val, req.query.num,req.query.page];
		let page = (param[3]-1)*param[2]
		let SQL
		if(param[0]==''||param[1]==''){
			SQL = `SELECT * FROM student limit ${page},${param[2]}`
		}else{
			SQL = `SELECT * FROM student WHERE ${param[0]} LIKE '%${param[1]}%' limit ${page},${param[2]}`
		}
		mysql.query(SQL, function (err, results) {
		    if (results) {
		        res.json({
		        	"list":results
		        })
		    }
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
	
})



//更新
/**
 * @api {post} /student/update 更新学生信息
 * @apiGroup student
 *
 * @apiParam {String} student_username 账号
 * @apiParam {String} student_password 密码
 * @apiParam {String} student_name 名字
 * @apiParam {String} student_grade 班级
 * @apiParam {String} student_email 邮箱
 * @apiParam {bool} student_state 状态
 * @apiParam {int} student_id 学生编号
 * @apiParamExample {json} Request-Example
 * {
 *  "student_username": "sssss"
 *  "student_password": "12312312"
 *  "student_name": "黄琳琪"
 *  "student_grade": "计科201704"
 *  "student_email": "812149408@qq.com"
 *  "student_state": 1
 *  "student_id": 4
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * }
 */

router.post('/update', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.body.student_id];
			if (req.body.student_state == '') {
				param.unshift('student_state')
			} else {
				param.unshift(`${req.body.student_state}`)
			}
			if (req.body.student_email == '') {
				param.unshift('student_email')
			} else {
				param.unshift(`'${req.body.student_email}'`)
			}
			if (req.body.student_grade == '') {
				param.unshift('student_grade')
			} else {
				param.unshift(`'${req.body.student_grade}'`)
			}
			if (req.body.student_name == '') {
				param.unshift('student_name')
			} else {
				param.unshift(`'${req.body.student_name}'`)
			}
			if (req.body.student_password == '') {
				param.unshift('student_password')
			} else {
				param.unshift(`'${req.body.student_password}'`)
			}
			if (req.body.student_username == '') {
				param.unshift('student_username')
			} else {
				param.unshift(`'${req.body.student_username}'`)
			}
			console.log(param);
			let SQL = `UPDATE student 
				set student_username=${param[0]},student_password=${param[1]},student_name=${param[2]},student_grade=${param[3]},student_email=${param[4]},student_state=${param[5]} 
				where student_id=${param[6]}`;
			console.log(SQL);
			mysql.query(SQL, function (err, results) {
				console.log(results);
				res.json()
				if (err) console.log("DELETE ERROR " + err);
			})
		
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
})


/**
 * @api {get} /student/delete 删除学生信息
 * @apiGroup student
 *
 * @apiParam {String} student_id 学生编号
 * @apiParamExample {json} Request-Example
 * {
 *  "student_id": 1
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * }
 */


router.get('/delete', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.student_id];
		let SQL = `UPDATE student set student_del=1 where student_id=${param[0]}`
		console.log(SQL);
		mysql.query(SQL, function (err, results) {
		    console.log(results);
		    res.json()
		    if (err) console.log("DELETE ERROR " + err);
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
    
})


/**
 * @api {post} /student/add 更新学生信息
 * @apiGroup student
 *
 * @apiParam {String} student_username 账号
 * @apiParam {String} student_password 密码
 * @apiParam {String} student_name 名字
 * @apiParam {String} student_grade 班级
 * @apiParam {String} student_email 邮箱
 * @apiParamExample {json} Request-Example
 * {
 *  "student_username": "sssss"
 *  "student_password": "12312312"
 *  "student_name": "黄琳琪"
 *  "student_grade": "计科201704"
 *  "student_email": "812149408@qq.com"
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * }
 */



router.post('/add', function (req, res) {
	const btoken = req.headers.authorization.split(' ').pop()
	if(token.token.tranToken(btoken)){
		let param = [req.body.student_username, req.body.student_password, req.body.student_name, req.body.student_grade, req.body.student_email];
		let SQL = `INSERT INTO student(student_username, student_password, student_name, student_grade, student_email) values('${param[0]}','${param[1]}','${param[2]}','${param[3]}','${param[4]}')`
		mysql.query(SQL, function (err, results) {
		    console.log(results);
		    res.json()
		    if (err) console.log("ADD BOOK ERROR " + err);
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
    
})


/**
 * @api {Get} /student/search 按条件查询学生信息
 * @apiGroup student
 *
 * @apiParam {String} condition 条件
 * @apiParam {String} val 内容
 * @apiParam {String} num 每页信息数目
 * @apiParamExample {json} Request-Example
 * {
 *  "condition": 'student_name'
 *  "val": '钱'
 *  "num": 3
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 *  "count": 2,
 *  "list": [
 *      {
 *          "student_id": 20170004,
 *          "student_username": "sew232",
 *          "student_password": "dasd34321",
 *          "student_name": "钱",
 *          "student_grade": "信息201902",
 *          "student_email": "21564564@qq.com",
 *          "student_state": 1,
 *          "student_del": 0
 *      },
 *      {
 *          "student_id": 20170005,
 *          "student_username": "dasuhd4",
 *          "student_password": "2daw23",
 *          "student_name": "钱三",
 *          "student_grade": "计科201701",
 *          "student_email": "454864@qq.com",
 *          "student_state": 1,
 *          "student_del": 0
 *      }
 *  ]
 * }
 */
router.get('/search', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.condition, req.query.val, req.query.num];
		let SQL = `SELECT * FROM student WHERE ${param[0]} LIKE '%${param[1]}%' limit 0,${param[2]}`
		let cSQL = `SELECT COUNT(*) AS count FROM student WHERE ${param[0]} LIKE '%${param[1]}%'`
		let count
		mysql.query(cSQL, function (err, results) {
		    if (results) {
				count = Math.ceil(results[0].count/param[2])
				mysql.query(SQL, function (err, results) {
				    if (results) {
						let redata = {
							"count":count,
							"list":results
						}
				        res.json(redata)
				    }
				})
		    }
		})   
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
	
})

module.exports = router