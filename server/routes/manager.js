var express = require('express');
var router = express.Router();
var token = require("../model/token")

//导入封装的数据库模块
var mysql = require("../mysql/Config");

//设置跨域访问
router.all("*",function(req,res,next){
    //设置允许跨域的域名，*代表允许任意域名跨域
    res.header("Access-Control-Allow-Origin","*");
    //允许的header类型
    res.header("Access-Control-Allow-Headers","content-type");
    //跨域允许的请求方式 
    res.header("Access-Control-Allow-Methods","DELETE,PUT,POST,GET,OPTIONS");
    if (req.method.toLowerCase() == 'options')
        res.send(200);  //让options尝试请求快速结束
    else
        next();
})


//定义路由

/**
 * @api {Get} /manager 获取第一页管理员信息
 * @apiGroup manager
 *
 * @apiParam {int} num 每页数量
 * @apiParamExample {json} Request-Example
 * {
 *  "num": 3
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 *  "count": 3,
 *  "list": [
 *      {
 *          "manage_id": 1,
 *          "manage_name": "张三",
 *          "manage_username": "manage1",
 *          "manage_password": "123123",
 *          "manage_del": 1
 *      },
 *      {
 *          "manage_id": 2,
 *          "manage_name": "王五",
 *          "manage_username": "s54",
 *          "manage_password": "123321",
 *          "manage_del": 1
 *      }
 *  ]
 * }
 */
router.get('/', function (req, res) {
	const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.num];
		let SQL = `SELECT * FROM manage limit 0,${param[0]}`
		let cSQL = `SELECT COUNT(*) AS count FROM manage`
		let count
		mysql.query(cSQL, function (err, results) {
		    if (results) {
				count = Math.ceil(results[0].count/param[0])
				mysql.query(SQL, function (err, results) {
				    if (results) {
				        res.json({
							"count":count,
							"list":results
						})
				    }
				})
		    }
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
	
    
})


/**
 * @api {Get} /manager/page 按页数获取管理员信息
 * @apiGroup manager
 *
 * @apiParam {String} condition 条件
 * @apiParam {String} val 内容
 * @apiParam {int} page 页数
 * @apiParam {int} num 每页数量
 * @apiParamExample {json} Request-Example
 * {
 *  "condition": 'manage_id'
 *  "val": '0'
 *  "page": 2
 *  "num": 3
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 *  "list": [
 *      {
 *          "manage_id": 1,
 *          "manage_name": "张三",
 *          "manage_username": "manage1",
 *          "manage_password": "123123",
 *          "manage_del": 1
 *      }
 *  ]
 * }
 */
router.get('/page', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.condition, req.query.val, req.query.num,req.query.page];
		let page = (param[3]-1)*param[2]
		let SQL
		if(param[0]==''||param[1]==''){
			SQL = `SELECT * FROM manage limit ${page},${param[2]}`
		}else{
			SQL = `SELECT * FROM manage WHERE ${param[0]} LIKE '%${param[1]}%' limit ${page},${param[2]}`
		}
		mysql.query(SQL, function (err, results) {
		    if (results) {
		        let redata = {
		        	"list":results
		        }
		        res.json(redata)
		    }
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
})


//更新
/**
 * @api {post} /manager/update 更新管理员信息
 * @apiGroup manager
 *
 * @apiParam {String} manage_name 管理员名字
 * @apiParam {String} manage_username 账号
 * @apiParam {String} manage_password 密码
 * @apiParam {int} manage_id 管理员编号
 * @apiParamExample {json} Request-Example
 * {
 *  "manage_name": "李四"
 *  "manage_username": "sss"
 *  "manage_password": "123321"
 *  "manage_id": 4
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * }
 */

router.post('/update', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.body.manage_id];
		if (req.body.manage_password == '') {
			param.unshift('manage_password')
		} else {
			param.unshift(`'${req.body.manage_password}'`)
		}
		if (req.body.manage_username == '') {
			param.unshift('manage_username')
		} else {
			param.unshift(`'${req.body.manage_username}'`)
		}
		if (req.body.manage_name == '') {
			param.unshift('manage_name')
		} else {
			param.unshift(`'${req.body.manage_name}'`)
		}
		let SQL = `UPDATE manage set manage_name=${param[0]},manage_username=${param[1]},manage_password=${param[2]} where manage_id=${param[3]}`;
		console.log(SQL);
		mysql.query(SQL, function (err, results) {
		    res.json()
		    if (err) console.log("DELETE ERROR " + err);
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
})

/**
 * @api {get} /manager/delete 删除管理员
 * @apiGroup manager
 *
 * @apiParam {String} manage_id 管理员编号
 * @apiParamExample {json} Request-Example
 * {
 *  "manage_id": 4
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * }
 */
router.get('/delete', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.manage_id];
		let SQL = `UPDATE manage set manage_del=1 where manage_id=${param[0]}`
		mysql.query(SQL, function (err, results) {
		    if (err) console.log("DELETE ERROR " + err);
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
})

/**
 * @api {post} /manager/add 添加管理员
 * @apiGroup manager
 *
 * @apiParam {String} manage_name 管理员名字
 * @apiParam {String} manage_username 账号
 * @apiParam {String} manage_password 密码
 * @apiParam {int} manage_id 管理员编号
 * @apiParamExample {json} Request-Example
 * {
 *  "manage_id": 4
 *  "manage_name": "李四"
 *  "manage_username": "sss"
 *  "manage_password": "123321"
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * }
 */
router.post('/add', function (req, res) {
	const btoken = req.headers.authorization.split(' ').pop()
	if(token.token.tranToken(btoken)){
		let param = [req.body.manage_name, req.body.manage_username, req.body.manage_password];
		let SQL = `INSERT INTO manage(manage_name, manage_username, manage_password) values('${param[0]}','${param[1]}','${param[2]}')`
		mysql.query(SQL, function (err, results) {
		    if (err) console.log("ADD BOOK ERROR " + err);
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
})

/**
 * @api {Get} /manager/search 按条件查询管理员信息
 * @apiGroup manager
 *
 * @apiParam {String} condition 条件
 * @apiParam {String} val 内容
 * @apiParam {String} num 每页信息数目
 * @apiParamExample {json} Request-Example
 * {
 *  "condition": 'manage_usename'
 *  "val": 'user2'
 *  "num": 3
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 *  "count": 2,
 *  "list": [
 *  ]
 * }
 */

router.get('/search', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.condition, req.query.val, req.query.num];
		let SQL = `SELECT * FROM manage WHERE ${param[0]} LIKE '%${param[1]}%' limit 0,${param[2]}`
		let cSQL = `SELECT COUNT(*) AS count FROM manage WHERE ${param[0]} LIKE '%${param[1]}%'`
		let count
		mysql.query(cSQL, function (err, results) {
		    if (results) {
				count = Math.ceil(results[0].count/param[2])
				mysql.query(SQL, function (err, results) {
				    if (results) {
				        res.json({
							"count":count,
							"list":results
						})
				    }
				})
		    }
		})   
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
	
})


module.exports = router