var express = require('express');
var router = express.Router();
var token = require('../model/token')
//导入封装的数据库模块
var mysql = require("../mysql/Config");


//定义路由

/**
 * @api {Get} /book 获取第一页的图书信息
 * @apiGroup book
 *
 * @apiParam {int} num 每页数量
 * @apiParamExample {json} Request-Example
 * {
 *  "num": 3
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * 	"count": 1,
 *  "list": [
 *      {
 *          "book_id": 1,
 *          "book_name": "python程序设计",
 *          "book_author": "吴惠茹",
 *          "book_publish": "机械工业出版社",
 *          "book_class": "计算机 教育",
 *          "book_exist": 11,
 *          "book_volume": 3,
 *          "book_remaining": 7,
 *          "book_del": 0
 *      },
 *      {
 *          "book_id": 2,
 *          "book_name": "JavaScript高级程序设计",
 *          "book_author": "马特·弗里斯比",
 *          "book_publish": "中国工信出版社",
 *          "book_class": "计算机 教育",
 *          "book_exist": 6,
 *          "book_volume": 2,
 *          "book_remaining": 4,
 *          "book_del": 0
 *      }
 *  ]
 * }
 */
router.get('/', function (req, res) {
	console.log(req.headers);
    const btoken = req.headers.authorization.split(' ').pop()
	//const btoken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXIyIiwiaWF0IjoxNjEzNjQwNjA2LCJleHAiOjE2MTM2NDc4MDZ9.hGPjf0EitLFNwMrFbnuglowQTZrLblVY3WWrshLj91c'
    if(token.token.tranToken(btoken)){
		let param = [req.query.num];
		let SQL = `SELECT * FROM book where book_del=0 limit 0,${param[0]}`
		let cSQL = `SELECT COUNT(*) AS count FROM book`
		let count
		mysql.query(cSQL, function (err, results) {
		    if (results) {
				count = Math.ceil(results[0].count/param[0])
				mysql.query(SQL, function (err, results) {
				    if (results) {
						let redata = {
							"count":count,
							"list":results
						}
				        res.json(redata)
				    }
				})
		    }
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	} 
})


/**
 * @api {Get} /book/page 按页数获取图书信息
 * @apiGroup book
 *
 * @apiParam {String} condition 条件
 * @apiParam {String} val 内容
 * @apiParam {int} page 页数
 * @apiParam {int} num 每页数量
 * @apiParamExample {json} Request-Example
 * {
 *  "condition": 'book_id'
 *  "val": '0'
 *  "page": 2
 *  "num": 3
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * 		    "list": [
 *      {
 *          "book_id": 1,
 *          "book_name": "python程序设计",
 *          "book_author": "吴惠茹",
 *          "book_publish": "机械工业出版社",
 *          "book_class": "计算机 教育",
 *          "book_exist": 11,
 *          "book_volume": 3,
 *          "book_remaining": 7,
 *          "book_del": 0
 *      },
 *      {
 *          "book_id": 2,
 *          "book_name": "JavaScript高级程序设计",
 *          "book_author": "马特·弗里斯比",
 *          "book_publish": "中国工信出版社",
 *          "book_class": "计算机 教育",
 *          "book_exist": 6,
 *          "book_volume": 2,
 *          "book_remaining": 4,
 *          "book_del": 0
 *      }
 *  ]
 * }
 */
router.get('/page', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.condition, req.query.val, req.query.num,req.query.page];
		let page = (param[3]-1)*param[2]
		let SQL
		if(param[0]==''||param[1]==''){
			SQL = `SELECT * FROM book limit ${page},${param[2]}`
		}else{
			SQL = `SELECT * FROM book WHERE ${param[0]} LIKE '%${param[1]}%' limit ${page},${param[2]}`
		}
		mysql.query(SQL, function (err, results) {
		    if (results) {
		        let redata = {
		        	"list":results
		        }
		        res.json(redata)
		    }
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
})




/**
 * @api {Get} /book/search 按条件查询学生信息
 * @apiGroup book
 *
 * @apiParam {String} condition 条件
 * @apiParam {String} val 内容
 * @apiParam {int} num 每页数据条数
 * @apiParamExample {json} Request-Example
 * {
 *  "condition": 'book_id',
 *  "val": '1',
 *  "num": 2
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 *  "count": 1,
 *  "list": [
 *      {
 *          "book_id": 1,
 *          "book_name": "python程序设计",
 *          "book_author": "吴惠茹",
 *          "book_publish": "机械工业出版社",
 *          "book_class": "计算机 教育",
 *          "book_exist": 11,
 *          "book_volume": 3,
 *          "book_remaining": 7,
 *          "book_del": 0
 *      }
 *  ]	
 * }
 */
router.get('/search', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.condition, req.query.val, req.query.num];
		let SQL = `SELECT * FROM book WHERE ${param[0]} LIKE '%${param[1]}%' limit 0,${param[2]}`
		let cSQL = `SELECT COUNT(*) AS count FROM book WHERE ${param[0]} LIKE '%${param[1]}%'`
		let count
		mysql.query(cSQL, function (err, results) {
		    if (results) {
				count = Math.ceil(results[0].count/param[2])
				mysql.query(SQL, function (err, results) {
				    if (results) {
						let redata = {
							"count":count,
							"list":results
						}
						//console.log(redata);
				        res.json(redata)
				    }
				})
		    }
		})   
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
})



//更新
/**
 * @api {post} /book/update 更新图书信息
 * @apiGroup book
 *
 * @apiParam {int} book_id 图书编号
 * @apiParam {String} book_name 图书名字
 * @apiParam {String} book_author 作者
 * @apiParam {String} book_publish 出版社
 * @apiParam {String} book_class 分类
 * @apiParam {int} book_exist 库存数目（包括借出的图书数目）
 * @apiParamExample {json} Request-Example
 * {
 *  "book_id": 1
 *  "book_name": "python程序设计"
 *  "book_author": "吴惠茹"
 *  "book_publish": "机械工业出版社"
 *  "book_class": "计算机 教育"
 *  "book_exist": 10
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * }
 */

router.post('/update', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.body.book_id];
		if (req.body.book_exist == '') {
			param.unshift('book_exist')
		} else {
			param.unshift(`${req.body.book_exist}`)
		}
		if (req.body.book_class == '') {
			param.unshift('book_class')
		} else {
			param.unshift(`'${req.body.book_class}'`)
		}
		if (req.body.book_publish == '') {
			param.unshift('book_publish')
		} else {
			param.unshift(`'${req.body.book_publish}'`)
		}
		if (req.body.book_author == '') {
			param.unshift('book_author')
		} else {
			param.unshift(`'${req.body.book_author}'`)
		}
		if (req.body.book_name == '') {
			param.unshift('book_name')
		} else {
			param.unshift(`'${req.body.book_name}'`)
		}
		
		let cSQL = `SELECT book_remaining,book_exist FROM book WHERE book_id='${param[5]}'`
		let remaining,change
		mysql.query(cSQL, function (err, results) {	
			if (param[4] != ''){
				change = parseInt(param[4])-results[0].book_exist
				remaining = results[0].book_remaining + change
				console.log(change,remaining);
				let SQL = `UPDATE book
					set book_name=${param[0]},book_author=${param[1]},book_publish=${param[2]},book_class=${param[3]},book_exist=${param[4]},book_remaining=${remaining}
					where book_id='${param[5]}'`
				mysql.query(SQL, function (err, results) {
				    res.json()
				    if (err) console.log("DELETE ERROR " + err);
				})
			}else{
				let SQL = `UPDATE book
					set book_name=${param[0]},book_author=${param[1]},book_publish=${param[2]},book_class=${param[3]},book_exist=${param[4]}
					where book_id='${param[5]}'`
				mysql.query(SQL, function (err, results) {
				    res.json()
				    if (err) console.log("DELETE ERROR " + err);
				})
			}
		    if (err) console.log("DELETE ERROR " + err);
			
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
})

//删除
/**
 * @api {get} /book/delete 删除图书
 * @apiGroup book
 *
 * @apiParam {int} book_id 图书编号
 * @apiParamExample {json} Request-Example
 * {
 *  "book_id": 1
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * }
 */


router.get('/delete', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.book_id];
		let SQL = `UPDATE book set book_del=1 where book_id='${param[0]}'`
		mysql.query(SQL, function (err, results) {
		    res.json()
		    if (err) console.log("DELETE ERROR " + err);
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
})

//添加
/**
 * @api {post} /book/add 添加图书
 * @apiGroup book
 *
 * @apiParam {String} book_name 图书名字
 * @apiParam {String} book_author 作者
 * @apiParam {String} book_publish 出版社
 * @apiParam {String} book_class 分类
 * @apiParam {int} book_exist 库存数目（包括借出的图书数目）
 * @apiParamExample {json} Request-Example
 * {
 *  "book_name": "python程序设计"
 *  "book_author": "吴惠茹"
 *  "book_publish": "机械工业出版社"
 *  "book_class": "计算机 教育"
 *  "book_exist": 10
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * }
 */


router.post('/add', function (req, res) {
	const btoken = req.headers.authorization.split(' ').pop()
	if(token.token.tranToken(btoken)){
		let param = [req.body.book_name, req.body.book_author, req.body.book_publish, req.body.book_class, req.body.book_exist];
		let SQL = `INSERT INTO book(book_name, book_author, book_publish, book_class, book_exist, book_remaining) values('${param[0]}','${param[1]}','${param[2]}','${param[3]}','${param[4]}','${param[4]}')`
		mysql.query(SQL, function (err, results) {
		    res.json()
		    if (err) console.log("ADD BOOK ERROR " + err);
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
})

//按借阅量排序获取
/**
 * @api {get} /book/hot 按借阅量排序获取
 * @apiGroup book
 * 
 * @apiSuccessExample  {json} Response-Example
 * {
 * [
 *     {
 *       "book_id": 1,
 *      "book_name": "python程序设计",
 *      "book_author": "吴惠茹",
 *      "book_publish": "机械工业出版社",
 *      "book_class": "计算机 教育",
 *      "book_exist": 11,
 *      "book_volume": 3,
 *      "book_remaining": 7,
 *      "book_del": 0
 *  },
 *  {
 *      "book_id": 2,
 *      "book_name": "JavaScript高级程序设计",
 *      "book_author": "马特·弗里斯比",
 *      "book_publish": "中国工信出版社",
 *      "book_class": "计算机 教育",
 *      "book_exist": 6,
 *      "book_volume": 2,
 *      "book_remaining": 4,
 *      "book_del": 0
 *  }
 * ]
 * }
 */



router.get('/hot', function (req, res) {
	const btoken = req.headers.authorization.split(' ').pop()
	if(token.token.tranToken(btoken)){
		let SQL = `SELECT * FROM book where book_del=0 order by book_volume desc`
		mysql.query(SQL, function (err, results) {
		    res.json(results)
		    if (err) console.log("DELETE ERROR " + err);
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
})




module.exports = router