const mysql = require('mysql');

module.exports = class Model {
	//链接数据库对象
	static conn = null
	//数据库连接方法
	static connection() {
		 Model.conn = mysql.createConnection({
		  host     : 'localhost',
		  user     : 'root',
		  password : 'root',
		  database : 'tsgl'
		});
		Model.conn.connect(err => {
			if(err){
				console.log(`数据库连接失败：${err.message}`);
			}
		});
	}
	static end() {
		if(null != Model.conn){
			Model.conn.end();
		}
	}
	
	/**
	 * 通用查询方法
	 * @param {string} sql 要执行的SQL语句  
	 * @param {Array} parms 给SQL语句的占位符进行赋值的参数数据  
	 * 
	 * */
	
	static query(sql,params = []){
		return new Promise((resolve,reject) => {
			this.connection()
			
			Model.conn.query(sql,params,(err,results) => {
				if(err){
					reject(err)
				}else{
					resolve(results)
				}
			})
		})
	}
}